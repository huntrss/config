-- basic configuration
-- tab configuration
vim.cmd("set expandtab")
vim.cmd("set tabstop=2")
vim.cmd("set softtabstop=2")
vim.cmd("set shiftwidth=2")
-- splitting windows
vim.opt.splitright = true
vim.opt.splitbelow = true
-- leader
vim.g.mapleader = " "
-- hybrid line number
vim.wo.number = true
vim.wo.relativenumber = true
-- format on write
--vim.cmd("autocmd BufWritePre * lua vim.lsp.buf.format()")
vim.api.nvim_create_autocmd("BufWritePre", { command = "lua vim.lsp.buf.format()" })
-- terminal
-- autocommand to always start in terminal mode
vim.api.nvim_create_autocmd({ "TermOpen" }, { command = "startinsert" })
-- keymap to open a terminal in a new window below
vim.keymap.set("n", "<leader>th", function()
	vim.cmd("7split term://fish")
end, { desc = "open [t]erminal [h]orizontally" })
vim.keymap.set("t", "<Esc>", "<C-\\><C-n>:close<CR>", { desc = "Leave terminal mode and close terminal window" })
vim.keymap.set("n", "<leader>tv", function()
	vim.cmd("75vs term://fish")
end, { desc = "open [t]erminal [v]ertically" })
-- keymaps for buffer handling
vim.keymap.set("n", "<leader>bd", ":bdelete<CR>", { desc = "[b]uffer: [d]elete" })
vim.keymap.set("n", "<leader>bn", ":bnext<CR>", { desc = "[b]uffer: [n]ext" })
vim.keymap.set("n", "<leader>bp", ":bprevious<CR>", { desc = "[b]uffer: [p]revious" })
--vim.keymap.set("n", "<leader>bo", [[:%bd|e#|bd#<CR>|'"]], { desc = "[b]uffer: delete [o]ther buffers" })
vim.keymap.set("n", "<leader>bo", function()
	-- copied from https://stackoverflow.com/a/77799027
	-- License according to StackOverlow: CC BY-SA 4.0
	-- Code created by Branko: https://stackoverflow.com/users/8196612/branko
	local current_buf = vim.fn.bufnr()
	local current_win = vim.fn.win_getid()
	local bufs = vim.fn.getbufinfo({ buflisted = 1 })
	for _, buf in ipairs(bufs) do
		if buf.bufnr ~= current_buf then
			vim.cmd("silent! bdelete " .. buf.bufnr)
		end
	end
	vim.fn.win_gotoid(current_win)
end, { desc = "[b]uffer: delete [o]ther buffers" })
