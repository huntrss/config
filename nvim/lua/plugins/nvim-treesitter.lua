return {
  "nvim-treesitter/nvim-treesitter",
  build = ":TSUpdate",
  config = function()
    -- Treesitter use git in order to avoid errors with default branches
    -- not named "master"
    require("nvim-treesitter.install").prefer_git = true

    -- required for alternative parser path
    vim.opt.runtimepath:append("$HOME/.local/share/nvim/tree-sitter/parsers/")
    -- change parser config for parser that don't use master as default
    -- branch
    local parser_config = require("nvim-treesitter.parsers").get_parser_configs()
    -- zig
    parser_config.zig = {
      install_info = {
        url = "https://github.com/maxxnino/tree-sitter-zig",
        files = {
          "src/parser.c",
        },
      },
      maintainers = { "@maxxnino" },
      branch = "main",
    }
    -- lua
    parser_config.lua = {
      install_info = {
        url = "https://github.com/MunifTanjim/tree-sitter-lua",
        files = {
          "src/parser.c",
          "src/scanner.c",
        },
      },
      maintainers = {
        "@muniftanjim",
      },
      branch = "main",
    }
    -- config
    local config = require("nvim-treesitter.configs")
    config.setup({
      ensure_installed = { "rust", "lua", "zig", "markdown_inline" },
      highlight = { enable = true },
      sync_install = false,
      indent = { enable = true },
      parser_install_dir = "$HOME/.local/share/nvim/tree-sitter/parsers/",
    })
  end,
}
