return {
	"mfussenegger/nvim-dap",
	dependencies = {
		"rcarriga/nvim-dap-ui",
	},
	config = function()
		local dap, dapui = require("dap"), require("dapui")
		dapui.setup({})

		dap.listeners.before.attach.dapui_config = function()
			dapui.open()
		end
		dap.listeners.before.launch.dapui_config = function()
			dapui.open()
		end
		dap.listeners.before.event_terminated.dapui_config = function()
			dapui.close()
		end
		dap.listeners.before.event_exited.dapui_config = function()
			dapui.close()
		end
		vim.keymap.set("n", "<Leader>dc", dap.continue, {desc = "[d]ebugging: [c]ontinue"})
		vim.keymap.set("n", "<Leader>ds", dap.step_over, {desc = "[d]ebugging: [s]tep over"})
		vim.keymap.set("n", "<Leader>di", dap.step_into, {desc = "[d]ebugging: step [i]nto"})
		vim.keymap.set("n", "<Leader>do", dap.step_out, {desc = "[d]ebugging: step [o]ut"})
		vim.keymap.set("n", "<Leader>db", dap.toggle_breakpoint, {desc = "[d]ebugging: toggle [b]reakpoint"})
		vim.keymap.set("n", "<Leader>db", dap.terminate, {desc = "[d]ebugging: [t]erminate debugging session"})
	end,
}
