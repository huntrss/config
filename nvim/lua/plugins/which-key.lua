return {
  "folke/which-key.nvim",
  event = "VeryLazy",
  init = function()
    vim.o.timeout = true
    vim.o.timeoutlen = 300
  end,
  opts = {},
  config = function()
    local wk = require("which-key")
    wk.register({
      ["<leader>d"] = { name = "[d]ebugging", _ = "which_key_ignore" },
      ["<leader>s"] = { name = "[s]earch", _ = "which_key_ignore" },
      ["<leader>c"] = { name = "[c]ode (language server actions)", _ = "which_key_ignore" },
      ["<leader>cg"] = { name = "[c]ode [g]o to", _ = "which_key_ignore" },
      ["<leader>t"] = { name = "[t]erminal", _ = "which_key_ignore" },
      ["<leader>b"] = { name = "[b]uffer", _ = "which_key_ignore" },
    })
  end,
}
