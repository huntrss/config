return {
  {
    "williamboman/mason.nvim",
    config = function()
      require("mason").setup()
    end,
  },
  {
    "williamboman/mason-lspconfig.nvim",
    config = function()
      require("mason-lspconfig").setup({
        ensure_installed = { "lua_ls", "rust_analyzer", "zls" },
      })
    end,
  },
  {
    "neovim/nvim-lspconfig",
    config = function()
      local capabilities = require("cmp_nvim_lsp").default_capabilities()
      local lspconfig = require("lspconfig")
      lspconfig.lua_ls.setup({
        capabilities = capabilities,
      })
      lspconfig.zls.setup({
        capabilities = capabilities,
      })
      vim.keymap.set("n", "<leader>cgD", vim.lsp.buf.declaration, { desc = "[c]ode: [g]o to [d]eclaration" })
      vim.keymap.set("n", "<leader>cgd", vim.lsp.buf.definition, { desc = "[c]ode: [g]o to [d]efinition" })
      vim.keymap.set("n", "<leader>ch", vim.lsp.buf.hover, { desc = "[c]ode: [h]over" })
      vim.keymap.set("n", "<leader>cgi", vim.lsp.buf.implementation, { desc = "[c]ode [g]o to [i]mplementation" })
      vim.keymap.set("n", "<leader>cs", vim.lsp.buf.signature_help, { desc = "[c]ode: [s]ignature help" })
      vim.keymap.set(
        "n",
        "<leader>cgt",
        vim.lsp.buf.type_definition,
        { desc = "[c]ode: [g]o to [t]ype definition" }
      )
      vim.keymap.set("n", "<leader>cr", vim.lsp.buf.rename, { desc = "[c]ode: [r]ename" })
      vim.keymap.set({ "n", "v" }, "<leader>ca", vim.lsp.buf.code_action, { desc = "[c]ode [a]ction" })
      vim.keymap.set("n", "<leader>cgr", vim.lsp.buf.references, { desc = "[c]ode: [g]o to references" })
      vim.keymap.set("n", "<leader>cf", vim.lsp.buf.format, { desc = "[c]ode: [f]ormat" })
    end,
  },
}
