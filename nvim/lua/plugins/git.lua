return {
	{
		"lewis6991/gitsigns.nvim",
		config = function()
			require("gitsigns").setup({})
			vim.keymap.set(
				"n",
				"<leader>vp",
				":Gitsigns preview_hunk_inline",
				{ desc = "[v]ersion control: [p]review hunk inline" }
			)
		end,
		"tpope/vim-fugitive",
	},
}
