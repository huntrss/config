# neovim configuration

This configuration was created based on this Youtube Video series:

[Neovim for newbs](https://www.youtube.com/watch?v=zHTeCSVAFNY&list=PLsz00TDipIffreIaUNk64KxTIkQaGguqn)

## Link

Some interesting links:

* [Rust and Neovim are AMAZING together](https://www.youtube.com/watch?v=gihHLsClHF0)
* [rustacean.nvim](https://github.com/mrcjkb/rustaceanvim/)
* [Automagically formatting on save, with Neovim and Language Server Protocol (LSP)](https://www.jvt.me/posts/2022/03/01/neovim-format-on-save/)
* [Neovim and Git: SOLVED](https://www.youtube.com/watch?v=zOQMwWqdp9w)
* [Transform Your Neovim into a IDE: A Step-by-Step Guide](https://martinlwx.github.io/en/config-neovim-from-scratch/)
* [Everything you need to know to configure neovim using lua](https://vonheikemen.github.io/devlog/tools/configuring-neovim-using-lua/)
